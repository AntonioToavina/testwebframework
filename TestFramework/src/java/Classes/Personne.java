/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

import Annotation.Url;
import ServiceController.ViewModel;
import java.util.HashMap;

/**
 *
 * @author antonio
 */

public class Personne {
    private int idPersonne;
    private String nom;
    private String prenom;
    private int age;

    /**
     * @return the idPersonne
     */
    public int getIdPersonne() {
        return idPersonne;
    }

    /**
     * @param idPersonne the idPersonne to set
     */
    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }
    
    @Url(link = "personne-info")
    public ViewModel getInfo_Personne(){
        ViewModel viewModel =new ViewModel();
        HashMap<String,Object> data = new HashMap<String,Object>();
        data.put("data", this);
        viewModel.setObject(data);
        viewModel.setRedirection("/InfoPersonne.jsp");
        
        return viewModel;
    }
    
}