<%-- 
    Document   : InfoPersonne
    Created on : 18 nov. 2022, 20:15:43
    Author     : antonio
--%>

<%@page import="java.util.HashMap"%>
<%@page import="Classes.Personne"%>

<%
    Personne personne=(Personne)((HashMap<String,Object>)request.getAttribute("value")).get("data");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Information de la personne</h1>
        <p>Nom : <%= personne.getNom() %></p>
        <p>Prenom : <%= personne.getPrenom()%></p>
        <p>Age : <%= personne.getAge() %></p>
    </body>
</html>
